-- Добавление пользователей
INSERT INTO consultation_db.user (full_name, birthday, email, role)
VALUES ('Иванов Иван', '1990-05-15', 'ivanov@example.com', 'USER'),
       ('Петров Петр', '1985-10-25', 'petrov@example.com', 'USER'),
       ('Сидорова Анна', '1995-03-07', 'sidorova@example.com', 'USER');

-- Добавление тегов
INSERT INTO consultation_db.tag (tag)
VALUES ('Java'),
       ('Python'),
       ('JavaScript');

-- Добавление менторов
INSERT INTO consultation_db.mentor (experience, how_can_help, skills, tag_id, user_id, image_path)
VALUES ('Опыт в Java более 5 лет', 'Помогу с освоением Spring Framework', 'Java, Spring, Hibernate', 1, 1, '/src/main/resources/static/images/mentor_1.jpg'),
       ('Опыт в Python более 3 лет', 'Помогу с изучением Django', 'Python, Django', 2, 2, '/src/main/resources/static/images/mentor_2.jpg'),
       ('Опыт в JavaScript более 4 лет', 'Помогу с изучением React', 'JavaScript, React', 3, 3, '/src/main/resources/static/images/mentor_3.jpg');

-- Добавление профессий
INSERT INTO consultation_db.catalog_professions (name)
VALUES ('Backend Developer'),
       ('Frontend Developer'),
       ('Full-stack Developer');

-- Связь менторов с профессиями
INSERT INTO consultation_db.mentor_has_catalog_professions (mentor_mentor_id, catalog_professions_id)
VALUES (1, 1),
       (2, 1),
       (3, 2);
