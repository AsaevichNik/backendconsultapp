package org.tinkoff.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.tinkoff.dto.mentor.MentorCreateEditDto;
import org.tinkoff.dto.mentor.MentorReadDto;
import org.tinkoff.mapper.mentor.MentorUserDto;
import org.tinkoff.service.MentorService;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/v1/mentor")
@RequiredArgsConstructor
@SecurityRequirement(name = "Keycloak")
public class MentorController {
    private final MentorService mentorService;
    @GetMapping("")
    @PreAuthorize("hasRole('admin') or hasRole('user')")
    public List<MentorReadDto> findAll() {
        return mentorService.findAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('admin') or hasRole('user')")
    public ResponseEntity<MentorUserDto> getMentorById(@PathVariable Integer id) {
        MentorUserDto mentorUserDto = mentorService.getMentorWithUser(id);
        return ResponseEntity.ok(mentorUserDto);
    }

    @PostMapping("")
    @PreAuthorize("hasRole('admin') or hasRole('user')")
    @ResponseStatus(HttpStatus.CREATED)
    public MentorReadDto create(MentorCreateEditDto mentor) {
        return mentorService.create(mentor);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('admin') or hasRole('user')")
    public MentorReadDto update(@PathVariable("id") Integer id,
                                @RequestBody MentorCreateEditDto mentorCreateEditDto){
        return mentorService
                .update(id, mentorCreateEditDto)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('admin') or hasRole('user')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Integer id) {
        if (!mentorService.delete(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/{mentorId}/image")
    @PreAuthorize("hasRole('admin') or hasRole('user')")
    public ResponseEntity<?> uploadImage(@PathVariable Integer mentorId, @RequestParam("file") MultipartFile file) {
        try {
            String imagePath = mentorService.saveImage(mentorId, file);
            return ResponseEntity.ok().body(imagePath);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to upload image");
        }
    }

    @GetMapping("/{mentorId}/image")
    @PreAuthorize("hasRole('admin') or hasRole('user')")
    public ResponseEntity<byte[]> getImage(@PathVariable Integer mentorId) {
        try {
            byte[] image = mentorService.getImage(mentorId);
            return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(image);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
}
