package org.tinkoff.database.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.tinkoff.database.entity.Mentor;

import java.util.List;
import java.util.Optional;


@Repository
public interface MentorRepository extends JpaRepository<Mentor, Integer> {
    @EntityGraph(attributePaths = {"mentorHasCatalogProfessions.catalogProfessions"})
    Optional<Mentor> findByMentorId(Integer id);
}
