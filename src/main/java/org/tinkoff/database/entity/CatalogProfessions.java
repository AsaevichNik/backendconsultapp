package org.tinkoff.database.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "catalog_professions")
public class CatalogProfessions {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false)
    private String name;

    @Builder.Default
    @OneToMany(mappedBy = "catalogProfessions")
    @JsonBackReference
    private List<MentorHasCatalogProfessions> mentorHasCatalogProfessions = new ArrayList<>();
}
