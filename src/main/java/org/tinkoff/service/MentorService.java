package org.tinkoff.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.tinkoff.database.entity.Mentor;
import org.tinkoff.database.entity.User;
import org.tinkoff.database.repository.MentorRepository;
import org.tinkoff.database.repository.UserRepository;
import org.tinkoff.dto.mentor.MentorCreateEditDto;
import org.tinkoff.dto.mentor.MentorReadDto;
import org.tinkoff.dto.user.UserReadDto;
import org.tinkoff.mapper.mentor.MentorCreateEditMapper;
import org.tinkoff.mapper.mentor.MentorReadMapper;
import org.tinkoff.mapper.mentor.MentorUserDto;
import org.tinkoff.mapper.user.UserReadMapper;

import javax.annotation.PostConstruct;
import javax.ws.rs.NotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class MentorService {
    private final MentorRepository mentorRepository;
    private final UserRepository userRepository;
    private final MentorCreateEditMapper mentorCreateEditMapper;
    private final MentorReadMapper mentorReadMapper;
    private final UserReadMapper userReadMapper;
    @Value("${image.directory}")
    private Path rootLocation;

    public Optional<MentorReadDto> findById(Integer id) {
        return mentorRepository
                .findById(id)
                .map(mentorReadMapper::map);
    }

    public List<MentorReadDto> findAll() {
        return mentorRepository
                .findAll()
                .stream()
                .map(mentorReadMapper::map)
                .toList();
    }

    @Transactional
    public MentorReadDto create(MentorCreateEditDto dto) {
        return Optional.of(dto)
                .map(mentorCreateEditMapper::map)
                .map(mentorRepository::save)
                .map(mentorReadMapper::map)
                .orElseThrow();
    }

    @Transactional
    public Optional<MentorReadDto> update(Integer id, MentorCreateEditDto dto) {
        return mentorRepository
                .findById(id)
                .map(mentor -> mentorCreateEditMapper.map(dto, mentor))
                .map(mentorRepository::saveAndFlush)
                .map(mentorReadMapper::map);
    }

    @Transactional
    public boolean delete(Integer id) {
        return mentorRepository.findById(id)
                .map(mentor -> {
                    mentorRepository.delete(mentor);
                    mentorRepository.flush();
                    return true;
                })
                .orElse(false);
    }

    @Transactional(readOnly = true)
    public MentorUserDto getMentorWithUser(Integer mentorId) {
        MentorReadDto mentor = mentorRepository.findByMentorId(mentorId)
                .map(mentorReadMapper::map)
                .orElseThrow(() -> new NotFoundException("Mentor not found"));
        UserReadDto user = userRepository.findById(mentor.getId())
                .map(userReadMapper::map)
                .orElseThrow(() -> new NotFoundException("User not found"));
        return new MentorUserDto(mentor, user);
    }

    public String saveImage(Integer mentorId, MultipartFile file) throws IOException {
        String filename = mentorId + "_" + file.getOriginalFilename();
        Path imagePath = this.rootLocation.resolve(filename);
        Files.copy(file.getInputStream(), imagePath);
        return imagePath.toString();
    }

    public byte[] getImage(Integer mentorId) throws IOException {
        Optional<Path> imagePath = findImagePath(mentorId);
        if (imagePath.isPresent()) {
            return Files.readAllBytes(imagePath.get());
        } else {
            throw new IOException("Image not found for mentor ID: " + mentorId);
        }
    }

    private Optional<Path> findImagePath(Integer mentorId) throws IOException {
        String fileNamePrefix = "mentor_" + mentorId;
        return Files.list(rootLocation)
                .filter(path -> path.getFileName().toString().startsWith(fileNamePrefix))
                .findFirst();
    }

}
