package org.tinkoff.mapper.mentor;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.tinkoff.dto.mentor.MentorReadDto;
import org.tinkoff.dto.user.UserReadDto;

@Data
@AllArgsConstructor
public class MentorUserDto {
    private MentorReadDto mentor;
    private UserReadDto user;
}